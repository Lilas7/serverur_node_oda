<?php
use Workerman\Worker;
use Workerman\WebServer;
use Workerman\Lib\Timer;
use PHPSocketIO\SocketIO;

include __DIR__ . '/vendor/autoload.php';
$sender_io = new SocketIO(2120);



$sender_io->on('workerStart', function(){
    
    $inner_http_worker = new Worker('http://0.0.0.0:2121');
    
    $inner_http_worker->onMessage = function($http_connection, $data){
 
        $_POST = $_POST ? $_POST : $_GET;
        global $sender_io;


        switch(@$_POST['action']){
            case 'next':
                $sender_io->emit('next', 'success' );

                return $http_connection->send( 'success' );

            case 'prev':
                $sender_io->emit('prev', 'success' );

                return $http_connection->send( 'success' );
        }


        return $http_connection->send('fail');
    };
    
    $inner_http_worker->listen();
});

if(!defined('GLOBAL_START'))
{
    Worker::runAll();
}
